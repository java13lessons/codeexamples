package additional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class ExampleCountItems {

	public static void main(String[] args) {

		List<Box> allBoxes = new ArrayList<Box>();
		allBoxes.add(new Box("Box3", 45));
		allBoxes.add(new Box("Box1", 78));
		allBoxes.add(new Box("Box3", 13));
		allBoxes.add(new Box("Box2", 5));
		allBoxes.add(new Box("Box2", 45));
		allBoxes.add(new Box("Box3", 3));
		allBoxes.add(new Box("Box2", 9));
		allBoxes.add(new Box("Box1", 15));
		allBoxes.add(new Box("Box2", 23));
		Collections.sort(allBoxes);

		Map<String, Integer> histogram = new TreeMap<String, Integer>();
		Iterator<Box> iterator = allBoxes.iterator();
		String currentName = "";
		int counter = 0;
		while (iterator.hasNext()) {
			Box currentBox = iterator.next();
			if (!currentName.equals(currentBox.getName())) {
				if (currentName != "")
					histogram.put(currentName, counter);
				currentName = currentBox.getName();
				counter = 0;
			}
			counter += currentBox.getCountOfItems();
		}

		if (currentName != "")
			histogram.put(currentName, counter);

		Iterator<Entry<String, Integer>> iteratorHistogram = histogram.entrySet().iterator();
		while (iteratorHistogram.hasNext()) {
			Entry<String, Integer> entry = iteratorHistogram.next();
			System.out.println("Name: " + entry.getKey() + " -->> " + entry.getValue());
		}

	}

}

class Box implements Comparable<Box> {

	private String name;
	private int countOfItems;

	public Box(String name, int countOfItems) {
		this.name = name;
		this.countOfItems = countOfItems;
	}

	@Override
	public int compareTo(Box o) {
		int result = 0;
		result = this.name.compareTo(o.name);
		if (result != 0)
			result = this.countOfItems - o.countOfItems;

		if (result > 0)
			return 1;
		else if (result < 0)
			return -1;
		else
			return 0;
	}

	public int getCountOfItems() {
		return countOfItems;
	}

	public String getName() {
		return name;
	}

	public void setCountOfItems(int countOfItems) {
		this.countOfItems = countOfItems;
	}

	public void setName(String name) {
		this.name = name;
	}

}
