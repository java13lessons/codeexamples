package additional;

import java.time.Duration;
import java.time.Instant;

public class ExampleVarags {

	public static void main(String[] args) {
///We provide the varargs

		/// Using varargs you provide the elements to the method as the parameters (you
		/// can provide as many parameters as you want)
		ExampleVarags.printEvenNumbers(3, 23, 1, 9, 3, 13, 2, 8);

		/// Otherwise, you need to pass the array
		int[] arr = { 3, 23, 1, 9, 3, 13, 2, 8 };
		System.out.print(System.lineSeparator());
		ExampleVarags.printEvenNumbers2(arr);
	}

	/// 1) YOU CAN'T HAVE MORE THAN ONE varags IN THE METHOD
	//// 2) Varags parameter should be defined at the end of definition (it should
	/// be the last one)
	/// ... stands for Variable arguments (Varargs)
	private static void printEvenNumbers(int... vars) {
		//// Varags can be used as the array (In fact, varags are considered as the
		//// array)
		for (int value : vars)
			if (value % 2 == 0)
				System.out.println(value);

	}

	private static void printEvenNumbers2(int[] vars) {

		for (int value : vars)
			if (value % 2 == 0)
				System.out.println(value);
	}

}
