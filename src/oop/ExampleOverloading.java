package oop;

public class ExampleOverloading {

	public static void main(String[] args) {

		System.out.println(multiplyThreeIntegers(2, 3, 2));
		System.out.println(multiplyThreeIntegers(2, 2));
		System.out.println(multiplyThreeIntegers(2));
	}

	private static int multiplyThreeIntegers(int a, int b, int c) {
		return a * b * c;
	}

	/// If I want to use default values
	/// The problem is that we can't actually determine which of a, b, c parameters
	/// are provided. We only that int int, or int int int, or int signature are
	/// provided

	private static int multiplyThreeIntegers(int a, int b) {
		return a * b * 1;
	}

	private static int multiplyThreeIntegers(int a) {
		return a * 1 * 1;
	}

}
