package oop;

public class PrivateConstructorExample {

	private String personName;

	//// In some cases, we need to do the validation to retric the creation of the
	//// objects
	private PrivateConstructorExample(String personName) {
		this.personName = personName;
	}

	public static PrivateConstructorExample getInstance(String personName) {
		// Need to check if the name is valid
		if (personName.length() > 0)
			return new PrivateConstructorExample(personName);
		else
			return null;/// if the value is not valid, we return null (we don't create the object)
	}
}
