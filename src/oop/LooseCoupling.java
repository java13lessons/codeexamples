package oop;

public class LooseCoupling {

	public static void main(String[] args) {
		TopicIntrfce topicObj = SeparateClass.getTopicObject();
		topicObj.understand();
	}

}

interface TopicIntrfce {

	void understand();
}

class TopicClass implements TopicIntrfce {
	@Override
	public void understand() {/// I can not do any changes related to the definision of the method directly
								/// from here, because there is the interface implememnted here
		System.out.println("I understand");
	}
}

class SeparateClass {
	public static TopicIntrfce getTopicObject() {
		return new TopicClass();
	}
}
