package oop;

public class Execution {

	public static void main(String[] args) {
//		Child obj = new Child("Janis");
//		System.out.println(obj.getName());

//		PrivateConstructorExample obj = new PrivateConstructorExample(); --- cannot do it, because the constructor of the class is private
		PrivateConstructorExample obj = PrivateConstructorExample.getInstance("Janis");/// this way is allowed
		System.out.println(obj);
	}

}
