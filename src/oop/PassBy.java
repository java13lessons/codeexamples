package oop;

public class PassBy {

	public static void main(String[] args) {
		int a = 4, b = 2;
		PassBy.changeValue(a, b);
		System.out.println(b);/// the value of b is not changed, because IT IS PASSED BY VALUE
		StringBuilder strBuilder = new StringBuilder("Here is the first part of the string");
		System.out.println(strBuilder.toString());

		String secondPart = " and here is the second part of the string";
		changeRefValue(strBuilder, secondPart);
		System.out.println(strBuilder.toString());

	}

	private static void changeValue(int aParam, int bParam) {
		bParam += aParam;//// here we work with the new variable aParam and bParam which are not related
							//// to the variables we pass to the method (because WE DO NOT PASS THE
							//// VARIABLE, WE PASS THE VALUES OF THEM - this is how pass by value works)
	}

	/// This case also works as PASS BY VALUE, but need to remember that the values
	/// of the reference types are the references, so we actually work with the
	/// reference, not with values of them
	private static void changeRefValue(StringBuilder strB, String str) {
		strB.append(str);/// append the string to the string builder
	}

}
