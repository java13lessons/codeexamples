package oop;

public class Child extends Parent {

	public Child() {

		/// Using super() method you can define which constructor from the superclass to
		/// be called
		/// if method super() is not called, then the program will try to call the
		/// constructor from the superclass, with the parameters, but if there is no
		/// such constructor in the superclass, you will have the compilation error
		super("Name");
		System.out.println("The contruct of Child class is called");
	}

	public Child(String name) {
		super(name);
	}

	public static void main(String[] args) {
		Child obj = new Child();//// The contructors from Child (sub-class) and Parent (superclass) will be
								//// called both
	}

}
