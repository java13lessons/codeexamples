package oop;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ExampleGenerics {

	public static void main(String[] args) {
		// I provide Integer type at the moment of the definition of the object and it
		// means that I can the T type used in the object as Integer only
//		Print<Integer> obj = new Print<Integer>();
//		obj.display(35);
////		obj.display("String"); ///can't do this, because String is not Integer
//		System.out.println(MaxValues.getMaximum(36, 21));////// System.out.println will work for any T type, because
//															////// REMEMBER THAT EACH OBJECT IN JAVA CAN BE CONVERTED TO
//															////// STRING (method toString() in Object class)
//		System.out.println(MaxValues.getMaximum("Aaaab", "Bbbbba"));

		System.out.println(MaxValues.getMaxumumNumber(34, 21));///// Ok
		System.out.println(MaxValues.getMaxumumNumber(34.3232, 21.3534));/// Ok for double
//		System.out.println(MaxValues.getMaxumumNumber(21.3534, 21)); //// Not ok, because the types must be the same
//       System.out.println(MaxValues.getMaxumumNumber("Abbsa","Bdasd")); //can't be used for Strings, because String is not the subclass of Number 

		A aObj = new A();
		B bObj = new B();
		C.testAny(13.2121, aObj);///// here it will work for any type, because it will consider the type as
		///// Object

		C.testParentSuper(aObj, bObj);

		BoxOfItems<Toy> boxOfToys = new BoxOfItems<Toy>();
		Bear bear = new Bear();
		boxOfToys.addItem(bear);
		Zebra zebra = new Zebra();
		boxOfToys.addItem(zebra);
		Computer computer = new Computer();
//		boxOfToys.addItem(computer);///can'ta add the computer to box of toys, because Computer is not the toy

		BoxOfItems<Electronics> boxOfTechs = new BoxOfItems<Electronics>();
		boxOfTechs.addItem(computer);

		//// Here is the trick/key of the generics - the same classes/methods can work
		//// differently for different objects

		List<B> listBs = new ArrayList<B>();

		///// The rules of the inheritance do not simple work for the usage of the
		///// collection
//		C.methodForClassA(listBs);////// Not this way, because we can only provide the List<A> to the method
	}

}

class Electronics {
	/// Implement the logic
}

class Computer extends Electronics {
	/// Implement the logic
}

class Zebra extends Toy {
	/// Implement the logic
}

class Bear extends Toy {
////Implement the logic
}

class Toy {
	//// Implement the logic
}

///Please remember, that WE CAN ONLY USE REFERENCE TYPES IN GENERICS ===>>>> that type T will always extend the class Object
class Print<T> {

	//// The type of the element is unknown
	public void display(T element) {
		System.out.println(element);
	}

}

class MaxValues {
	//// It's not mandatory to define the generic types at the beginning, we can
	//// also use the, in static methods

	//// If just define generic type this way, it means that you can only do the
	//// same action as you can you with the variables defined as Object.
//	public static <T> T getMaximum(T a, T b) { ///// .... <T> <---- the generic type which will be used in the method
//		T max = a;
//		return max;
//	}

	///// For generics "extends" works for "extends" and for "implements" both (in
	///// terms of inheritance)
	/////// This means that the type T must implement the interface Comparable
	//// PLEASE, NOTE THAT a and b should have the same type
	public static <T extends Comparable<T>> T getMaximum(T a, T b) { ///// .... <T> <---- the generic type which will be
																		///// used in the method

		if (a.compareTo(b) > 0)/// please, remember - if a.compareTo(b) returns positive value, it means that a
								/// > b
			return a;
		else if (a.compareTo(b) < 0)
			return b;
		else /// a.compareTo(b) == 0
			return a;
	}

	/// If we want to compare the Number only
	public static <T extends Number & Comparable<T>> T getMaxumumNumber(T a, T b) {

		if (a.compareTo(b) > 0)
			return a;
		else if (a.compareTo(b) < 0)
			return b;
		else
			return a;
	}

	//// If we define the numbers as List<Number>, we won't be able to provide the
	//// list Integer

	/// If we use the question it actually means that the type is one of those which
	/// extend ex. Number
	public static Number getMaxValueFromList(List<? extends Number> numbers) {
		Number maxValue = null;
		Iterator<? extends Number> iterator = numbers.iterator();
		//// here is the logic
		return maxValue;
	}

}

////This is the class, which represents the box of items with one specific type T
class BoxOfItems<T> {

	private List<T> items = new ArrayList<T>();

	public void addItem(T item) {
		this.items.add(item);
	}

	public void removeItem(T item) {
		this.items.remove(item);
	}

	public void showItems() {
		Iterator<T> iterator = this.items.iterator();
		while (iterator.hasNext())
			System.out.println(iterator.next());
	}

}

class A {
}

class B extends A {
}

class C {
	public static <T> void testAny(T a, T b) {
	}

	public static <T extends A> void testParentSuper(T a, T b) {
	}

	public static void methodForClassA(List<A> list) {
		//// Write the logi here
	}

}
