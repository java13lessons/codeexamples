package oop;

public class Coupling {

	public static void main(String[] args) {
		Box b = new Box(2, 4, 3);
		System.out.println(b.volume);

	}

}

///Tight coupling example (not good design)
class Box {
	public int volume;/// the private parameter is not used here

	public Box(int length, int width, int height) {
		this.volume = length * width * height;
	}
}

class Subject {

	Topic t = new Topic();

	public void startReading() {
//		t.understand(); // in case there are any changes done here, we will need to cover the changes in
		// both classes
	}

}

class Topic {
	public void understand(String topicName) {
		System.out.println("I understand " + topicName);
	}
}
