package oop;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

///Every time you create the class which can be cloned, you need to "implement" Cloneable interface
////Cloneable is the marker interface - these are the interfaces, which do not have any methods (so we don't need to implement anything)

///If we try to clone the object of the class which "is not marked as cloneable", then there will be the exception CloneNotSupportedException thrown
public class Virus implements Cloneable {

	private Virus predecessor;//// define the object, which points to the predecssor of the virus
	private List<Virus> succesors = new ArrayList<Virus>();
	private Person owner;

	public void constructPredecessorsText(StringBuilder str) {
		if (this.predecessor != null) {
			str.append(this.predecessor.getOwner().getName() + " " + this.predecessor.getOwner().getSurname());
			str.append(",");
			this.predecessor.constructPredecessorsText(str);
		}
	}

	public String getAllPredecessorsText() {
		StringBuilder strBuilder = new StringBuilder();
		this.constructPredecessorsText(strBuilder);//// I pass the object, so this should work as "pass by-reference"
		return strBuilder.toString();
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}

	public Person getOwner() {
		return owner;
	}

	public void setSuccesors(List<Virus> succesors) {
		this.succesors = succesors;
	}

	public List<Virus> getSuccesors() {
		return succesors;
	}

	public Virus getPredecessor() {
		return predecessor;
	}

	public void setPredecessor(Virus predecessor) {
		this.predecessor = predecessor;
	}

	private String getSuccessorsText() {
		StringBuilder strBuild = new StringBuilder();
		String delim = "";
		Iterator<Virus> iterator = this.succesors.iterator();
		while (iterator.hasNext()) {
			strBuild.append(delim);
			Person owner = iterator.next().getOwner();
			strBuild.append(owner.getName());
			strBuild.append(" ");
			strBuild.append(owner.getSurname());
			if (delim.equals(""))
				delim = ", ";
		}
		return strBuild.toString();
	}

	@Override
	public String toString() {
		/// Return the information about the virus
		return "The owner of the virus is " + this.owner.getName() + " " + this.owner.getSurname()
				+ ", the predecossers of the virus are: " + this.getAllPredecessorsText()
				+ " the successors of the virus are:" + this.getSuccessorsText();
	}

	public static void main(String[] args) throws CloneNotSupportedException {

		Person person1 = new Person("Jack", "Beck");
		Person person2 = new Person("Misbah", "Tucker");
		Person person3 = new Person("Clayton", "Ramsay");
		Person person4 = new Person("Tomi", "Washington");
		Person person5 = new Person("Amari", "Barlow");

		System.out.println(person2);

		Virus virus = new Virus();
		person1.setVirus(virus);
		virus.setOwner(person1);

		person1.meetAnotherPerson(person5);
		System.out.println(person5);

		person5.meetAnotherPerson(person4);

		person4.meetAnotherPerson(person3);
		person4.meetAnotherPerson(person2);
		System.out.println("The virus: " + person4.getVirus());

	}

	@Override
	public Virus clone() throws CloneNotSupportedException {
		Virus newVirus = (Virus) super.clone();//// In this case we don't to call the method from the superclass,
												//// because we still need to change all the parameters we copy
		/// When clone happens in the superclass, it creates the copy of the object
		/// itself, but IT DOES NOT
		/// CREATE THE COPY OF THE ATTRIBUTES OF THE OBJECTS
		/// Need to correct it.

		//// 1.Change the predecessor
		newVirus.setPredecessor(this);
		//// 2.Copy the list and add a new value to the current one
		//// Note:We can get Succesors from the new object (virus) or from the current
		//// one (this), there is no difference, because they both are the same object
		newVirus.setSuccesors(new ArrayList<Virus>());
		this.getSuccesors().add(newVirus);
		return newVirus;
	}

}

class Person {

	private String name;
	private String surname;
	private Virus virus = null;

	@Override
	public String toString() {
		return "Name: " + this.name + ", Surname: " + this.surname
				+ (this.virus != null ? ", who has the virus" : ", who has no virus");
	}

	public void setVirus(Virus virus) {
		this.virus = virus;
	}

	public void meetAnotherPerson(Person person) throws CloneNotSupportedException {
		if (this.virus != null && person.getVirus() == null) {///// if one person has the virus,
			//// he/she can spread to another person, but only if the opposite person does
			//// not have the virus
			person.setVirus(this.virus.clone());
			person.getVirus().setOwner(person);
		} else if (this.virus == null && person.getVirus() != null) {
			this.setVirus(person.getVirus().clone());
			this.getVirus().setOwner(this);
		}
	}

	public Virus getVirus() {
		return virus;
	}

	public Person(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

}
