package oop;

public class Parent {

	private String name;

	public Parent() {
		System.out.println("The contructor of Parent class is called");
	}

	//// The class (the object) can only be constructed using the name
	public Parent(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
