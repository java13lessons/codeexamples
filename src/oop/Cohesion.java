package oop;

import java.util.ArrayList;
import java.util.List;

public class Cohesion {

}

class JobStatus {
	private String field;
	private Date startDate;
	private String name;

	public void setField(String field) {
		this.field = field;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getField() {
		return field;
	}

	public String getName() {
		return name;
	}

	public Date getStartDate() {
		return startDate;
	}
}

class PersonCoh {

	private Date dateOfBirth;
	private List<PhoneNumber> phoneNumbers = new ArrayList<PhoneNumber>();
	private String name, surname;
	private JobStatus job;

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public JobStatus getJob() {
		return job;
	}

	public String getName() {
		return name;
	}

	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}

	public String getSurname() {
		return surname;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setJob(JobStatus job) {
		this.job = job;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

}

class PhoneNumber {
	private String type;
	private int number;

	public PhoneNumber(String type, int number) {
		this.type = type;
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	public String getType() {
		return type;
	}
}

class Date {
	private int month;
	private int day;
	private int year;

	public Date(int month, int day, int year) {
		this.month = month;
		this.day = day;
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

}