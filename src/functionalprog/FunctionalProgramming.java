package functionalprog;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class FunctionalProgramming {

	public static void main(String[] args) {

		List<String> names = new ArrayList<String>();
		names.add("Jack");
		names.add("Peter");
		names.add("Eric");
		names.add("James");
		names.add("Robert");

		// THIS IS FINE
//		for (String name : names)
//			System.out.println(name);

//		names.forEach(name -> actionsForEveryElement(name));
		
		List<String> uppercaseNames = new ArrayList<String>();
//		for (String name : names)
//			uppercaseNames.add(name.toUpperCase());
//
//		for (String name : uppercaseNames)
//			System.out.println(name);

		names.stream().map(String::toUpperCase).forEach(name -> {
			uppercaseNames.add(name);
			System.out.println(name);
		});

		List<String> listOfIntegers = new ArrayList<String>();
		listOfIntegers.add("46");
		listOfIntegers.add("13");
		listOfIntegers.add("52");
		listOfIntegers.add("16");
		listOfIntegers.add("5");

		List<Integer> integers = new ArrayList<Integer>();
		listOfIntegers.stream().map(Integer::parseInt).forEach(element -> integers.add(element));

		/// Filter out all odd numbers
		integers.stream().filter(number -> number % 2 == 0).forEach(number -> System.out.println(number));

	}

	private static void actionsForEveryElement(String name) {
		System.out.println(name);
		System.out.println("This is the name " + name);
	}

}
