package collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class TestExampleArrayList {

	public static void main(String[] args) {

		List<String> list = new ArrayList<String>();
//		list.add("Element1");
//		list.add("Element3");
//		list.add("Element2");
//		printList(list);

		//// For Linked lists - the usage is the same, but the performance of the
		//// operations is different
		list = new LinkedList<String>();
		list.add("Element1");
		list.add("Element3");
		list.add("Element2");
		printList(list);
	}

	private static void printList(List<String> list) {
//		Iterator<String> iterator = list.iterator();
//		while (iterator.hasNext())
//			System.out.println(iterator.next());

//		boolean alreadyAdded = false;
		ListIterator<String> listIterator = list.listIterator();
		while (listIterator.hasNext()) {
			System.out.println(listIterator.next());
//			if (!alreadyAdded) {
			listIterator.remove();//// if we remove and add the element, then we can replace the element this way
			listIterator.add("AddedElement");//// the element is added to the next position
//				alreadyAdded = true;
//		}
		}

		boolean alreadyAdded = false;

		while (listIterator.hasPrevious()) {
			System.out.println(listIterator.previous());
			if (!alreadyAdded) {
				listIterator.add("AddedElementBack");
				alreadyAdded = true;
			}
		}
	}

}
