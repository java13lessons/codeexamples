package collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class TestMaps {

	public static void main(String[] args) {
		Map<Integer, String> persons = new TreeMap<Integer, String>();
//		Map<Integer, String> persons = new HashMap<Integer, String>();
		persons.put(1, "Name1");
		persons.put(3, "Name2");
		persons.put(2, "Name3");
		persons.put(4, "Name3");
		persons.put(10, "Name9");
		persons.put(13, "Name9");
		persons.put(11, "Name3");
		persons.put(15, "Name3");
		persons.put(9, "Name7");
		persons.put(6, "Name2");
		persons.put(5, "Name3");
		printElements(persons);
		
		
	}

	private static void printElements(Map<Integer, String> elements) {
		Set<Entry<Integer, String>> entries = elements.entrySet();
		Iterator<Entry<Integer, String>> iterator = entries.iterator();
		while (iterator.hasNext()) {
			Entry<Integer, String> entry = iterator.next();
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
		}
	}

}
