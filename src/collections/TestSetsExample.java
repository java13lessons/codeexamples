package collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class TestSetsExample {

	public static void main(String[] args) {
		/// Sets usually needed when we have more elements
//		Set<String> set = new TreeSet<String>();
//		Set<String> set = new HashSet<String>();
//		set.add("Element1");
//		set.add("Element3");
//		set.add("Element2");
//		set.add("Element21");
//		set.add("Element14");
//		set.add("Element18");
//		set.add("Element13");
//		printSet(set);
		
		Set<Person> personsSet = new TreeSet<Person>();
		personsSet.add(new Person("Name1", "Surname1"));
		personsSet.add(new Person("Name2", "Surname2"));
	}

	private static void printSet(Set<String> set) {
		Iterator<String> iterator = set.iterator();
		while (iterator.hasNext())
			System.out.println(iterator.next());
	}

}
