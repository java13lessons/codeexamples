package collections;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.sql.*;

public class TestPerformance {

	private Map<String, String> data = new TreeMap<String, String>();
	private List<String> keysToRead = new ArrayList<String>();
	private static final int NUMBER_OF_ENTRIES = 2000;
	private static final int NUMBER_OF_VALUES_TO_READ = 25;
	private static final String FILEPATH = "C:\\Users\\Arturs Olekss\\Documents\\JAVA_lessons\\160h\\CollectionsTest\\200K.csv";

	public void putDataToDatabase() {
		Connection con = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/java13_2", "root", "");
			con.setAutoCommit(false);

			Iterator<Entry<String, String>> iterator = data.entrySet().iterator();

			final String STATEMENT = "INSERT INTO `test_table` (`id`,`name`) values (?,?)";
			while (iterator.hasNext()) {
				Entry<String, String> entry = iterator.next();
				PreparedStatement stmt = con.prepareStatement(STATEMENT);
				stmt.setString(1, entry.getKey());
				stmt.setString(2, entry.getValue());
				stmt.executeUpdate();
			}
			con.commit();
			con.close();

		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		TestPerformance obj = new TestPerformance();
		obj.fillData();
		obj.putDataToDatabase();
//		obj.printAllElements();

//		/// Start time
//		long startTime = System.nanoTime();
//		obj.performRead();
//		long endTime = System.nanoTime();
////		obj.performRead();
//		long totalTime = endTime - startTime;
//		System.out.println("Total time " + totalTime);

	}

	public void performRead() {
//		data.get("a81a7ca1-10c5-47f2-b9b2-7ed9039f7585");
//		data.get("2500aa67-94b7-4c9d-9f4c-08c6f4a087d7");
//		data.get("1AkJd8jh7wjQUfXNrz56DBSZ2hj2ap3cs");
//		data.get("1CGKTYgLg8B9N48sVmjm932bb5fg16EJer");
//		data.get("1JMC9BhH8QsZt8v7nGYp87o3PCx8RiQ4Mi");
		Iterator<String> iterator = this.keysToRead.iterator();
		while (iterator.hasNext())
			this.data.get(iterator.next());
	}

	public void printAllElements() {
		Set<Entry<String, String>> entries = data.entrySet();
		Iterator<Entry<String, String>> iterator = entries.iterator();
		while (iterator.hasNext()) {
			Entry<String, String> entry = iterator.next();
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
		}
	}

	public void fillData() throws Exception {
//		System.out.println("Enter file path:");

//		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//		String filePath = reader.readLine();

		String filePath = FILEPATH;
		String fileContent = getFileContent(filePath);

		String[] lines = fileContent.split(System.lineSeparator());

		StringTokenizer columnTokenizer = new StringTokenizer(lines[0], ",");
		String[] columnnames = new String[columnTokenizer.countTokens()];

		int index = 0;
		while (columnTokenizer.hasMoreElements()) {
			columnnames[index] = columnTokenizer.nextToken();
			index++;
		}

		List<String> allKeys = new ArrayList<String>();

		for (int i = 1; i < lines.length && i < NUMBER_OF_ENTRIES; i++) {////
			String line = lines[i];
			StringTokenizer elements = new StringTokenizer(line, ",");

			index = 0;
			String id = null;
			String name = null;
			while (elements.hasMoreElements()) {
				String elementValue = elements.nextToken();
				switch (columnnames[index]) {
				case "id":
					id = elementValue;
					break;
				case "Name":
					name = elementValue;
					break;
				}
				index++;
			}
			this.data.put(id, name);
			allKeys.add(id);
		}

		/// Shuffle the list
		Collections.shuffle(allKeys);

		/// Get first NUMBER_OF_VALUES_TO_READ entries
		for (int i = 0; i < NUMBER_OF_VALUES_TO_READ; i++)
			this.keysToRead.add(allKeys.get(i));

	}

	private String getFileContent(String filePath) throws Exception {
		File file = new File(filePath);
		BufferedReader reader = new BufferedReader(new FileReader(file, StandardCharsets.UTF_8));

		StringBuffer content = new StringBuffer();
		String line = reader.readLine();/// read the first line
		while (line != null) {
			content.append(line);
			content.append(System.lineSeparator());
			line = reader.readLine();/// read the next line
		}

		reader.close();
		return content.toString();
	}

}
