package collections;

public class Person implements Comparable<Person> {

	private String name;
	private String surname;

	@Override
	public int compareTo(Person o) {
		String str1 = this.name + this.surname;
		String str2 = o.getName() + o.getSurname();
		return str1.compareTo(str2);
	}

	public Person(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

}
