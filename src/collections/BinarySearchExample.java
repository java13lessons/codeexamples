package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class BinarySearchExample {

	public static void main(String[] args) {
		List<Integer> elements = new ArrayList<Integer>();
		elements.add(83);
		elements.add(144);
		elements.add(943);
		elements.add(48);
		elements.add(39);
		elements.add(10);
		elements.add(49);
		elements.add(24);
		elements.add(93);
		elements.add(123);
		elements.add(38);
		elements.add(78);
		elements.add(13);
		elements.add(19);
		elements.add(27);
		elements.add(29);
		elements.add(68);
		elements.add(74);
		elements.add(91);
		Collections.sort(elements);
		printList(elements);

		binarySearch(elements, 29);
	}

	private static void printList(List<Integer> list) {
		ListIterator<Integer> listIterator = list.listIterator();
		while (listIterator.hasNext())
			System.out.println(listIterator.next());
	}

	private static void binarySearch(List<Integer> elements, int elementSearch) {
		if (elements.size() == 1) {
			if (elements.get(0) == elementSearch)
				System.out.println("Element found, it is located at the 0 position");
			else
				System.out.println("Elements is not found");
			return;
		}

		boolean elementFound = false;
//		int positionFound = -1;//// if the element is not found, than the position will be -1
		int numberOfStepsNeeded = 0;
		int positionShift = 0;
		while (!elementFound) {
			numberOfStepsNeeded++;
			/// Need to get middle element first
			int middlePosition = (int) Math.ceil((double) elements.size() / 2);
			if (middlePosition == 1) {//// this case may happen if we have 3 or 2 elements

				if (elements.get(1) == elementSearch) {
					System.out.println("Element found at position 1" + (positionShift + 2));
					elementFound = true;
				}

				else if (elements.get(0) == elementSearch) {
					System.out.println("Element found at position " + (positionShift + 1));
					elementFound = true;
				} else {
					break;
				}

			} else if (elementSearch > elements.get(middlePosition - 1)) {
				positionShift += middlePosition - 1;
				elements = elements.subList(middlePosition, elements.size());
			} else if (elementSearch < elements.get(middlePosition - 1))
				elements = elements.subList(0, middlePosition - 1);
			else {/// element found
				System.out.println("Element found at position " + (positionShift + middlePosition - 1));
				elementFound = true;
			}

		}
		if (!elementFound)
			System.out.println("Element not found");
		System.out.println("Number of steps: " + numberOfStepsNeeded);

	}

}
