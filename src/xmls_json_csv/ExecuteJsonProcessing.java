package xmls_json_csv;

import java.util.Iterator;

public class ExecuteJsonProcessing {

	public static void main(String[] args) {
		try {
			ReadFile fileReader = new ReadFile();
			String jsonContent = fileReader.getFileContent();
			FileToMenu fileToMenu = new JsonToMenu();
			BreakfastMenu breakfastMenu = fileToMenu.getMenu(jsonContent);

			Iterator<Food> iterator = breakfastMenu.getFoods().iterator();
			while (iterator.hasNext())
				System.out.println(iterator.next() + System.lineSeparator());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
