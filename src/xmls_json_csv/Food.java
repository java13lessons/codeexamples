package xmls_json_csv;

public class Food {

	private String name;
	private double price;
	private String description;
	private int calories;

	//// In this case, it is easier to fill the attributes using Setters, not
	//// constructor

	public void setCalories(int calories) {
		this.calories = calories;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getCalories() {
		return calories;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}

	@Override
	public String toString() {
		StringBuffer food = new StringBuffer();
		food.append("Food name: ");
		food.append(this.name + System.lineSeparator());
		food.append("Food price: ");
		food.append(this.price + "$" + System.lineSeparator());
		food.append("Food description: ");
		food.append(this.description + System.lineSeparator());
		food.append("Food calories: ");
		food.append(this.calories + " kcal");

		return food.toString();
	}

}
