package xmls_json_csv;

import java.util.Iterator;

import org.json.*;

public class JsonToMenu implements FileToMenu {

	@Override
	public BreakfastMenu getMenu(String fileContent) {

		BreakfastMenu breakFastMenu = new BreakfastMenu();

		JSONObject breakFastJsonObj = new JSONObject(fileContent);
		JSONArray foodsJsonArray = breakFastJsonObj.getJSONArray("foods");
		Iterator<Object> iterator = foodsJsonArray.iterator();
		while (iterator.hasNext()) {
			JSONObject foodElementJson = (JSONObject) iterator.next();
			Food food = new Food();
			food.setName(foodElementJson.getString("name"));
			food.setPrice(Double.parseDouble(foodElementJson.getString("price").replace('$', '\0')));
			food.setDescription(foodElementJson.getString("description"));
			food.setCalories(foodElementJson.getInt("calories"));
			breakFastMenu.setFood(food);
		}

		return breakFastMenu;
	}

}
