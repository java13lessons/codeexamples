package xmls_json_csv;

import java.util.Iterator;

public class ReadCsvFile {

	public static void main(String[] args) {
		try {
			ReadFile fileReader = new ReadFile();
			String csvContent = fileReader.getFileContent();
			FileToMenu fileToMenu = new CsvToMenu();
			BreakfastMenu breakfastMenu = fileToMenu.getMenu(csvContent);

			Iterator<Food> iterator = breakfastMenu.getFoods().iterator();
			while (iterator.hasNext())
				System.out.println(iterator.next() + System.lineSeparator());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
