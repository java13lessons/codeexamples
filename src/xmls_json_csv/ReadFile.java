package xmls_json_csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class ReadFile {

	public String getFileContent() throws Exception {
		File file = this.readFile();
		BufferedReader reader = new BufferedReader(new FileReader(file, StandardCharsets.UTF_8));

		StringBuffer content = new StringBuffer();
		String line = reader.readLine();/// read the first line
		while (line != null) {
			content.append(line);
			content.append(System.lineSeparator());
			line = reader.readLine();/// read the next line
		}

		reader.close();
		return content.toString();

	}

	private File readFile() throws Exception {
		System.out.println("Enter the filepath:");

		/// The alternative to sacnner, since we want to read one line only
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String filePath = reader.readLine();
		return new File(filePath);
	}

}
