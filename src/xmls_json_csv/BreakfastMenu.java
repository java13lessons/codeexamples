package xmls_json_csv;

import java.util.ArrayList;
import java.util.List;

public class BreakfastMenu {

	private final List<Food> foods = new ArrayList<Food>();

	public List<Food> getFoods() {
		return foods;
	}

	public void setFood(Food food) {
		this.foods.add(food);
	}

}
