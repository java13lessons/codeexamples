package xmls_json_csv;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XmlToMenu implements FileToMenu {

	@Override
	public BreakfastMenu getMenu(String fileContent) {//// fileContent is the text content of XML file
														//// provided
		BreakfastMenu breakfastMenu = new BreakfastMenu();
		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document xmlDocument = builder.parse(new InputSource(new StringReader(fileContent)));
			processXmlFile(xmlDocument, breakfastMenu);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return breakfastMenu;
	}

	private void processXmlFile(Document xmlDocument, BreakfastMenu breakfastMenu) {
////Based on the structure of Xml document, the logic below is written
		Element breakfastRootElement = xmlDocument.getDocumentElement();///// this is how we can get the root element of
		NodeList foodsList = breakfastRootElement.getElementsByTagName("food"); ///// the XML document

		/// Need to go through all the food elements of the document and fetch the
		/// values
		/// from there
		for (int i = 0; i < foodsList.getLength(); i++) {
			Food food = new Food();
			Element foodElement = (Element) foodsList.item(i);

			food.setName(foodElement.getAttribute("name"));
			NodeList foodProperties = foodElement.getChildNodes();
			//// Go through all the parameters of the food element in xml
			for (int j = 0; j < foodProperties.getLength(); j++) {
				Node parameter = foodProperties.item(j);
				/// Then we need to detect, which element we currently process
				switch (parameter.getNodeName()) {/// this will return the name of the element
//				case "name":
//					food.setName(parameter.getTextContent());
//					break;
				case "price":
					food.setPrice(Double.parseDouble(parameter.getTextContent().replace('$', '\0')));
					break;
				case "description":
					food.setDescription(parameter.getTextContent());
					break;
				case "calories":
					food.setCalories(Integer.parseInt(parameter.getTextContent()));
					break;
				default:
					break;
				}
			}
			breakfastMenu.setFood(food);

		}

	}

}
