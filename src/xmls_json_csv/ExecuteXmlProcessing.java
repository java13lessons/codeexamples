package xmls_json_csv;

import java.util.Iterator;

public class ExecuteXmlProcessing {

	public static void main(String[] args) {
		try {
			ReadFile fileReader = new ReadFile();
			String xmlContent = fileReader.getFileContent();
			XmlToMenu xmlProcessing = new XmlToMenu();
			BreakfastMenu breakfastMenu = xmlProcessing.getMenu(xmlContent);

			Iterator<Food> iterator = breakfastMenu.getFoods().iterator();
			while (iterator.hasNext())
				System.out.println(iterator.next() + System.lineSeparator());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
