package xmls_json_csv;

import java.util.StringTokenizer;

public class CsvToMenu implements FileToMenu {

	@Override
	public BreakfastMenu getMenu(String fileContent) {
		String[] lines = fileContent.split(System.lineSeparator());
		/// The first line represents the names of the columns
		StringTokenizer columnTokenizer = new StringTokenizer(lines[0], ",");
		String[] columnnames = new String[columnTokenizer.countTokens()];/// columnTokenizer.countTokens() - will return
																			/// the number of the columns

		/// Let's go through the string tokenizer and write the columns
		int index = 0;
		while (columnTokenizer.hasMoreElements()) {
			columnnames[index] = columnTokenizer.nextToken();
			index++;
		}

		/// Go through the lines and get the content
		BreakfastMenu breakFastMenu = new BreakfastMenu();

		for (int i = 1; i < lines.length; i++) {
			String line = lines[i];
			StringTokenizer foodElements = new StringTokenizer(line, ",");

			index = 0;
			Food food = new Food();
			while (foodElements.hasMoreElements()) {
				String elementValue = foodElements.nextToken();
				switch (columnnames[index]) {
				case "name":
					food.setName(elementValue);
					break;
				case "description":
					food.setDescription(elementValue);
					break;
				case "price":
					food.setPrice(Double.parseDouble(elementValue.replace('$', '\0')));
					break;
				case "calories":
					food.setCalories(Integer.parseInt(elementValue));
					break;
				}
				index++;
			}
			breakFastMenu.setFood(food);
		}

		return breakFastMenu;
	}

}
