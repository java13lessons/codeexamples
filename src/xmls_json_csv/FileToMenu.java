package xmls_json_csv;

public interface FileToMenu {

	public BreakfastMenu getMenu(String fileContent);
}
