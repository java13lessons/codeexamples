package xmls_json_csv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;

import org.json.JSONObject;

public class CreateFile {

	public static void main(String[] args) throws Exception {

		BreakfastMenu breakFastMenu = new BreakfastMenu();

		Food porridge = new Food();

		porridge.setName("Porridge");
		porridge.setDescription("Porridge (historically also spelled porage, porrige, or parritch) "
				+ "is a food commonly eaten as a breakfast cereal dish, "
				+ "made by boiling ground, crushed or chopped starchy plants�typically "
				+ "grain�in milk. It is often cooked or served with added flavourings such as sugar, "
				+ "honey, (dried) fruit or syrup to make a sweet cereal, or it can be mixed with spices, "
				+ "meat or vegetables to make a savoury dish. It is usually served hot in a bowl, "
				+ "depending on its consistency");
		porridge.setCalories(400);
		porridge.setPrice(1);
		breakFastMenu.setFood(porridge);

		Food pancakes = new Food();
		pancakes.setName("Pancakes");
		pancakes.setDescription(
				"A pancake is a flat cake, often thin " + "and round, prepared " + "from a starch-based batter that may"
						+ " contain eggs, milk and butter and cooked on a hot surface such "
						+ "as a griddle or frying pan, often frying with oil or butter.");
		pancakes.setCalories(600);
		pancakes.setPrice(2.20);
		breakFastMenu.setFood(pancakes);

		JSONObject breakFastMenuJson = new JSONObject(breakFastMenu);

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//		System.out.println("Enter the folder name for JSON:");

		String filePath = "";
//		reader.readLine() + "/GenratedMenu.json";
		BufferedWriter writer = null;
//				new BufferedWriter(new FileWriter(filePath));
//		writer.write(breakFastMenuJson.toString());
//		writer.close();

		System.out.println("Enter the folder name for XML:");

		filePath = reader.readLine() + "/GenratedMenu.xml";
		writer = new BufferedWriter(new FileWriter(filePath));
		MenuToXml menuToXmlObj = new MenuToXml();
		writer.write(menuToXmlObj.getXmlContent(breakFastMenu));
		reader.close();
		writer.close();

	}

}
