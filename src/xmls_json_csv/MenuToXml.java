package xmls_json_csv;

import java.io.StringWriter;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class MenuToXml {

	public String getXmlContent(BreakfastMenu breakfastMenu)
			throws ParserConfigurationException, TransformerConfigurationException, TransformerException {

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = docFactory.newDocumentBuilder();
		Document document = documentBuilder.newDocument();
		/// Now we will build the XMl document;

		Element breakFastElement = document.createElement("breakfast_menu");
		document.appendChild(breakFastElement);

		Iterator<Food> iterator = breakfastMenu.getFoods().iterator();
		while (iterator.hasNext()) {
			Food foodObj = iterator.next();
			Element foodElement = document.createElement("food");
			foodElement.setAttribute("name", foodObj.getName());
			breakFastElement.appendChild(foodElement);

			Element priceElement = document.createElement("price");
			priceElement.setTextContent("$" + foodObj.getPrice());
			foodElement.appendChild(priceElement);

			Element descriptionElement = document.createElement("description");
			descriptionElement.setTextContent(foodObj.getDescription());
			foodElement.appendChild(descriptionElement);

			Element caloriesElement = document.createElement("calories");
			caloriesElement.setTextContent(Integer.toString(foodObj.getCalories()));
			foodElement.appendChild(caloriesElement);
		}
		DOMSource domSource = new DOMSource(document);
		StringWriter writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		///These settings are used for the formating
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		
		transformer.transform(domSource, result);

		return writer.toString();
	}

}
