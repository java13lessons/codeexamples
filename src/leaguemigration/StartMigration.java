package leaguemigration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class StartMigration {

	public static void main(String[] args) throws Exception {
		System.out.println("Starting Teams migration");

		System.out.println("Enter the filepath:");

		/// The alternative to scanner, since we want to read one line only
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String filePath = reader.readLine();

		File file = new File(filePath);
		reader = new BufferedReader(new FileReader(file, StandardCharsets.UTF_8));

		StringBuffer content = new StringBuffer();
		String line = reader.readLine();/// read the first line
		while (line != null) {
			content.append(line);
			content.append(System.lineSeparator());
			line = reader.readLine();/// read the next line
		}

		reader.close();

//		MigrateTeams teamsMigration = new MigrateTeams();
//		teamsMigration.migrate(content.toString());
		
		Migration leagueTableMigration = new MigrateTable();
		leagueTableMigration.migrate(content.toString());
	}

}
