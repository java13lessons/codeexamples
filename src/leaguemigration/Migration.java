package leaguemigration;

public interface Migration {

	public void migrate(String teamsCSV);

}
