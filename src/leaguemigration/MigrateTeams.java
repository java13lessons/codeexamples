package leaguemigration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class MigrateTeams implements Migration {

	@Override
	public void migrate(String teamsCSV) {
		String[] teamsArray = teamsCSV.split("\n");

		Connection con = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/league", "root", "");
			con.setAutoCommit(false);
			for (String team : teamsArray) {
				PreparedStatement stmt = con.prepareStatement("insert into `teams` (`name`) VALUES (?)");
				stmt.setString(1, team);
				stmt.execute();
			}
			con.commit();
		}

		catch (Exception e) {
			e.printStackTrace();
			System.err.println("Migration failed");
		}

	}

}
