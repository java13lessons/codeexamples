package leaguemigration;

public class LeagueTableRow {

	/// If we have so many elements, this is not the best to put the all as the
	/// parmeters of the constructor
	private int position, teamID, gamesPlayed, wins, draws, loses, goalDiff, points;

	public void setDraws(int draws) {
		this.draws = draws;
	}

	public void setGamesPlayed(int gamesPlayed) {
		this.gamesPlayed = gamesPlayed;
	}

	public void setGoalDiff(int goalDiff) {
		this.goalDiff = goalDiff;
	}

	public void setLoses(int loses) {
		this.loses = loses;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setTeamID(int teamID) {
		this.teamID = teamID;
	}

	public void setWins(int wins) {
		this.wins = wins;
	}

	public int getDraws() {
		return draws;
	}

	public int getGamesPlayed() {
		return gamesPlayed;
	}

	public int getGoalDiff() {
		return goalDiff;
	}

	public int getLoses() {
		return loses;
	}

	public int getPoints() {
		return points;
	}

	public int getPosition() {
		return position;
	}

	public int getTeamID() {
		return teamID;
	}

	public int getWins() {
		return wins;
	}

}
