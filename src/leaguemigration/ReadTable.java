package leaguemigration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import dnl.utils.text.table.TextTable;

public class ReadTable {

	private Map<Integer, String> teamMap = new TreeMap<Integer, String>();
	private Connection con = null;

	public static void main(String[] args) {

		ReadTable rt = new ReadTable();
		rt.showLeagueTable();
		
	}

	private Object[][] transformArrayList(List<Object[]> list) {
		Object[][] arr = new Object[list.size()][list.get(0).length];
		for (int i = 0; i < list.size(); i++)
			arr[i] = list.get(i);

		return arr;
	}

	public void showLeagueTable() {

		List<LeagueTableRow> tableRows = this.getTableRows();

		String[] columnNames = new String[8];
		columnNames[0] = "Position";
		columnNames[1] = "Team";
		columnNames[2] = "Games played";
		columnNames[3] = "Wins";
		columnNames[4] = "Draws";
		columnNames[5] = "Loses";
		columnNames[6] = "Goal difference";
		columnNames[7] = "Points";

		/// We use Object here, to be able to store different types of the values (ex.
		/// String or Integer) in the table
		List<Object[]> data = new ArrayList<Object[]>();
		Iterator<LeagueTableRow> iterator = tableRows.iterator();
		while (iterator.hasNext()) {
			LeagueTableRow row = iterator.next();
			Object[] columnValues = new Object[8];

			columnValues[0] = row.getPosition();
			columnValues[1] = this.teamMap.get(row.getTeamID());
			columnValues[2] = row.getGamesPlayed();
			columnValues[3] = row.getWins();
			columnValues[4] = row.getDraws();
			columnValues[5] = row.getLoses();
			columnValues[6] = row.getGoalDiff();
			columnValues[7] = row.getPoints();
			data.add(columnValues);

		}

		TextTable tt = new TextTable(columnNames, this.transformArrayList(data));
		tt.printTable();

	}

	private void setTeamMapping() throws Exception {
		ResultSet results = this.con.createStatement().executeQuery("SELECT * FROM `teams`");
		while (results.next())
			this.teamMap.put(results.getInt(1), results.getString(2));

	}

	private List<LeagueTableRow> getTableRows() {
		List<LeagueTableRow> tableRows = new ArrayList<LeagueTableRow>();

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");

			this.con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/league", "root", "");
			//// No need for autocommit here, because we won't change the data in the
			//// database

			this.setTeamMapping();
			ResultSet results = this.con.createStatement().executeQuery("SELECT * FROM `league_table`");///// instead of
																										///// selection
																										///// the team
																										///// id we can
																										///// also use
																										///// INNER JOIN
			while (results.next()) {
				LeagueTableRow tableRow = new LeagueTableRow();
				tableRow.setPosition(results.getInt(1));
				tableRow.setTeamID(results.getInt(2));
				tableRow.setGamesPlayed(results.getInt(3));
				tableRow.setWins(results.getInt(4));
				tableRow.setDraws(results.getInt(5));
				tableRow.setLoses(results.getInt(6));
				tableRow.setGoalDiff(results.getInt(7));
				tableRow.setPoints(results.getInt(8));
				tableRows.add(tableRow);
			}

			this.con.close();
		}

		catch (Exception e) {
			// TODO: handle exception
		}

		return tableRows;
	}

}
