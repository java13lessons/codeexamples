package leaguemigration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class MigrateTable implements Migration {

	private Connection con = null;
	private TreeMap<String, Integer> teamNameIdMap = new TreeMap<String, Integer>();

	private int getTeamId(String teamName) {
		return this.teamNameIdMap.get(teamName);
	}

	private void setTeamIds() throws Exception {

		PreparedStatement stmt = this.con.prepareStatement("select * from `teams`");
		ResultSet results = stmt.executeQuery();
		while (results.next()) {
			this.teamNameIdMap.put(results.getString(2), results.getInt(1));
		}
	}

	private void setTableRows(List<LeagueTableRow> tableRows) throws Exception {

		Iterator<LeagueTableRow> iterator = tableRows.iterator();
		while (iterator.hasNext()) {
			LeagueTableRow currentRow = iterator.next();
//			PreparedStatement stmt = this.con
//					.prepareStatement("insert into `league_table` (`position`,`team`,`games_played`,"
//							+ "`wins`,`draws`," + "`loses`,`goal_difference`,`points`) values (?,?,?,?,?,?,?,?)");

			PreparedStatement stmt = this.con
					.prepareStatement("update `league_table` set `team` = ?,`games_played` = ?," + "`wins` = ?,`draws` = ?,"
							+ "`loses` = ?,`goal_difference` = ?,`points` = ?  where `position` = ?");
			stmt.setInt(8, currentRow.getPosition());
			stmt.setInt(1, currentRow.getTeamID());
			stmt.setInt(2, currentRow.getGamesPlayed());
			stmt.setInt(3, currentRow.getWins());
			stmt.setInt(4, currentRow.getDraws());
			stmt.setInt(5, currentRow.getLoses());
			stmt.setInt(6, currentRow.getGoalDiff());
			stmt.setInt(7, currentRow.getPoints());
			stmt.executeUpdate();
		}

	}

	@Override
	public void migrate(String teamsCSV) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			this.con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/league", "root", "");
			this.con.setAutoCommit(false);
			this.setTeamIds();

			String[] rowsArray = teamsCSV.split("\n");

			StringTokenizer strToken = new StringTokenizer(rowsArray[0], ",");
			String[] columns = new String[strToken.countTokens()];
			int index = 0;
			while (strToken.hasMoreElements()) {
				columns[index] = strToken.nextToken();
				index++;
			}

			List<LeagueTableRow> leagueTableRows = new ArrayList<LeagueTableRow>();
			for (int i = 1; i < rowsArray.length; i++) {
				String currentRow = rowsArray[i];
				strToken = new StringTokenizer(currentRow, ",");

				LeagueTableRow tableRow = new LeagueTableRow();
				leagueTableRows.add(tableRow);//// it doesn't matter at which point we add the element to the arraylist

				index = 0;
				while (strToken.hasMoreElements()) {
					String value = strToken.nextToken();
					String currentColumn = columns[index].replace("[^a-zA-Z0-9 -]", "");
					
					boolean resultBool = currentColumn.equals("Position");
					switch (currentColumn) {
//					case "Position":
//						tableRow.setPosition(Integer.parseInt(value));
//						break;
//					case "Team":
//						tableRow.setPosition(this.getTeamId(value));
//						break;
					case "Games played":
						tableRow.setGamesPlayed(Integer.parseInt(value));
						break;
					case "Wins":
						tableRow.setWins(Integer.parseInt(value));
						break;
					case "Draws":
						tableRow.setDraws(Integer.parseInt(value));
						break;
					case "Loses":
						tableRow.setLoses(Integer.parseInt(value));
						break;
					case "Goal Difference":
						tableRow.setGoalDiff(Integer.parseInt(value));
						break;
					case "Points":
						tableRow.setPoints(Integer.parseInt(value));
						break;
					default:
						break;
					}
					if (index == 0)
						tableRow.setPosition(Integer.parseInt(value));
					else if (index == 1)
						tableRow.setTeamID(this.getTeamId(value));
					else if (index == 7)
						tableRow.setPoints(Integer.parseInt(value.replace("\r", "")));

					index++;
				}
			}
			this.setTableRows(leagueTableRows);
			this.con.commit();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Migration failed");
		}
	}

}
