package abstraction;

///If we implement the interfaces in the abstract classes, then we don't (mandatory) need to implement the methods from them 

///In fact, when we implement the interfaces and do implement the methods from them - we can consider them as abstract methods
public abstract class PC implements Computer {

	private boolean isOn = false;

	@Override
	public void powerOff() {
		this.isOn = false;
	}

	@Override
	public void powerOn() {
		Computer.super.powerOn();/// this will call the implementation of powerOn default method in Computer
									/// interface
		this.isOn = true;
	}

	public abstract double getSceenSize();

}
