package abstraction;

public class HP extends PC {

	private Monitor monitor;

	@Override
	public double getSceenSize() {
		return this.monitor.size();
	}

}