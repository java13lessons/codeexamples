package abstraction;

public class TestInnerClass {

	public static void main(String[] args) {
		InnerClassExample outerClass = new InnerClassExample();

		/// The object from the inner class should be created from the object of the
		/// outer class
//		InnerClassExample.InnerClass innerClass = outerClass.new InnerClass(12);
//		innerClass.printValue();/// after the object is created, we can use it as the simple
//		outerClass.setElement(16);
//		innerClass.printSum();

		Object obj = outerClass.getInstance();/// if the inner class is defined as the private class, we there is a
												/// crazy situation: 1)We can actually have this object and use it, but
												/// we can't access the type of the inner

		/// Here I can create the object of inner class, without using the object of the
		/// outer class
		InnerClassExample.InnerClassStatic innerStaticClass = new InnerClassExample.InnerClassStatic(19);
		innerStaticClass.printValue();
	}

}
