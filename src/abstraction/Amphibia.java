package abstraction;

public class Amphibia implements Shark, Human {

	//// If we have duplicate method error, than we must redefine/override this
	//// method
	@Override
	public void move() {
		Shark.super.move();
		Human.super.move();
	}
}
