package abstraction;

public class IPhone implements Smarthphone {

	private boolean on = false;

	/// The interesting thing is that for the compiler there is no difference what
	/// is the name of the variable (parameter) of the method which comes from the
	/// superclass or the interface
	public void call(int phone) {
		System.out.println("I'm calling to " + phone);
	};

	@Override
	public void powerOn() {
		this.on = true;
	}

	@Override
	public void powerOff() {
		this.on = false;
	}

}
