package abstraction;

////At first we will have the errors, which point to us that we need to implement abstract methods
public class Circle extends Shape {

	private double radius;

	public Circle(double radius) {
		this.radius = radius;
	}

	@Override
	public double getArea() {
		return Math.PI * this.radius * this.radius;
	}

	@Override
	public double getPerimeter() {
		return 2 * Math.PI * this.radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", my radius is " + this.radius;
	}

}
