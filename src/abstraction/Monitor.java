package abstraction;

public class Monitor implements Computer {

	private boolean isOn = false;
	private double length;
	private double width;

	public Monitor(double length, double width) {
		this.length = length;
		this.width = width;
	}
	
	public double size() {
		return Math.sqrt((this.length * this.length) + (this.width * this.width));///Pythagorean theorem
	}

	@Override
	public void powerOff() {
		this.isOn = false;
	}

	@Override
	public void powerOn() {
		this.isOn = true;
	}

	public boolean isOn() {
		return isOn;
	}

}
