package abstraction;

public class Rectangle extends Shape implements Drawable {

	private double length;
	private double width;

	public Rectangle(double length, double width) {
		this.length = length;
		this.width = width;
	}

	@Override
	public double getArea() {
		return this.length * this.width;
	}

	@Override
	public double getPerimeter() {
		return 2 * (this.length + this.width);
	}

	@Override
	public String toString() {
		return super.toString() + ", my length is " + this.length + ", my width is " + this.width;
	}

	//// draw the rectangle using dots
	@Override
	public void draw() {
		/// Draw the first line
		for (int j = 0; j < this.width; j++)
			System.out.print(".");

		System.out.print(System.lineSeparator());
		/// Draw the lines from the second to the last - 1
		for (int i = 1; i < this.length - 1; i++) {//// responsible for looping through the lines
			System.out.print(".");
			for (int j = 1; j < this.width - 1; j++)/// responsible loop through the columns
				System.out.print(" ");
			System.out.print(".");
			System.out.print(System.lineSeparator());
		}
		/// Draw the last line
		for (int j = 0; j < this.width; j++)
			System.out.print(".");

	}

}
