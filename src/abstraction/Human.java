package abstraction;

public interface Human extends Animal {

	@Override
	default void move() {
		System.out.println("I'm walking");
	}

}
