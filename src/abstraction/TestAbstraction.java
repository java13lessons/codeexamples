package abstraction;

public class TestAbstraction {

	public static void main(String[] args) {
//		Shape shape = new Shape(); you cannot create the Shape object
		Shape shape1 = new Circle(5);
		shape1.setColor("yellow");
		TestAbstraction.printInfoAboutShape(shape1);
		System.out.print(System.lineSeparator());

		Shape shape2 = new Rectangle(2, 5);
		shape2.setColor("green");
		TestAbstraction.printInfoAboutShape(shape2);
		System.out.print(System.lineSeparator());

		Shape shape3 = new Triangle(13.2, 4.3, 10.3, 3.4);
		shape3.setColor("black");
		TestAbstraction.printInfoAboutShape(shape3);
		System.out.print(System.lineSeparator());

		double var = 4;
		/// Anonymous Class
		Shape shape4 = new Shape() {
			//// Square
			private double edgeLen = var;/// you can initialize the variable with the values you can access in the
											/// method
			///// In Anonymous classes you don't create the constructors, because the
			///// definition of these will already be used for the objec;

			@Override
			public double getPerimeter() {
				return 4 * this.edgeLen;
			}

			@Override
			public double getArea() {
				return this.edgeLen * this.edgeLen;
			}

			@Override
			public String toString() {
				return super.toString() + ", my edge is " + this.edgeLen + " units long";
			}
		};
		shape4.setColor("orange");

		TestAbstraction.printInfoAboutShape(shape4);//// the anonymous classes have no names

		/// Shape type does implement Drawable, that's why we need to use Rectangle or
		/// Drawable as the type
		Drawable drawableShape1 = new Rectangle(5, 3);
		drawableShape1.draw();
		System.out.print(System.lineSeparator());

		Drawable drawableShape2 = new Rectangle(10, 15);
		drawableShape2.draw();

		//// We can create the anymous classes from the interfaces the same way as we
		//// can do with the Abstract classes
		Drawable drawableElement = new Drawable() {

			@Override
			public void draw() {
				// TODO Auto-generated method stub

			}
		};
	}

	/// The method below does not care about what is the actual type of the shape
	/// provided
	private static void printInfoAboutShape(Shape shape) {
		System.out.println("The area of the " + shape.getClass().getSimpleName() + " is " + shape.getArea());
		System.out.println("The perimeter of the " + shape.getClass().getSimpleName() + " is " + shape.getPerimeter());
		System.out.println(shape);
	}

}
