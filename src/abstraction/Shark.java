package abstraction;

public interface Shark extends Animal {

	@Override
	default void move() {
		System.out.println("I'm swimming");
	}
}
