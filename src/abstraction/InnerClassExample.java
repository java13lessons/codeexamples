package abstraction;

public class InnerClassExample {

	private int element;

	public int getElement() {
		return element;
	}

	public void setElement(int element) {
		this.element = element;
	}

	////// In this we can see that the class can be used outside the class, but we
	////// have no option to instanciate it directly from outside
	public InnerClass getInstance() {
		return new InnerClass(element);
	}

	//// We can create one class inside another
	/// Please, note that if we define the inner class this way, IT IS A PART OF THE
	//// OBJECT of InnerClassExample (outer class)
	private class InnerClass {
		private double value;

		public InnerClass(int value) {
			this.value = value;
		}

		public double getValue() {
			return value;
		}

		public void printValue() {
			System.out.println(this.value);
		}

		public void printSum() {

//			System.out.println(this.value + element);/// from inner class we can access the element of the outer class
			System.out.println(this.value + InnerClassExample.this.element);//// this would be the same as above
		}
	}

	///// The same way as for the attributes and the methods of the outer class,
	///// inner classes can also be static ===>>>> WE DON'T NEED TO CREATE THE
	///// OBJECT OF THE OUTER CLASS TO ACCESS/USE THEM
	public static class InnerClassStatic {
		private double value;

		public InnerClassStatic(int value) {
			this.value = value;
		}

		public double getValue() {
			return value;
		}

		public void printValue() {
			System.out.println(this.value);
		}

//		public void printSum() {
///// Since the inner class is static, it does not have the outer object
////			System.out.println(this.value + InnerClassExample.this.element);//// this would be the same as above
//		
//		}
	}

}
