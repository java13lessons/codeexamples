package abstraction;

public class Triangle extends Shape {

	private double[] edgesLen = new double[3];
	private double height;

	public Triangle(double edge1, double edge2, double edge3, double height) {
		this.edgesLen[0] = edge1;
		this.edgesLen[1] = edge2;
		this.edgesLen[2] = edge3;
		this.height = height;
	}

	public double getArea() {
		return this.edgesLen[0] * this.height * 0.5;
	};

	@Override
	public double getPerimeter() {
		return this.edgesLen[0] + this.edgesLen[1] + this.edgesLen[2];
	}

	@Override
	public String toString() {
		return super.toString() + ", my edges are: " + this.edgesLen[0] + "," + this.edgesLen[1] + ","
				+ this.edgesLen[2];
	}

}
