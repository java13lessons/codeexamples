package abstraction;

public abstract class Shape {

	private String color;

	public abstract double getArea();///// we define the method as abstract, because there is no common formula for
										///// all the shapes. Each shape has the different implementation

	public abstract double getPerimeter();

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "I'm " + this.getClass().getSimpleName() + ", I'm colored in " + this.color;
	}

}
