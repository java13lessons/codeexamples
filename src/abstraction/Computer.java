package abstraction;

public interface Computer {
	default void powerOn() {
		System.out.println("Computer is turning on");
	};

	void powerOff();
}
