package exceptions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MyLogingClass {

	private String logName;
	private List<Message> messages = new ArrayList<Message>();

	public MyLogingClass(String logName) {
		this.logName = logName;
		this.addMessage(MessageTypes.INFO, "The start of log " + this.logName);
	}

	public String getLogName() {
		return logName;
	}

	public void addMessage(char messageType, String messageText) {
		this.messages.add(new Message(messageType, messageText));
	}

	public void addEndMessage() {
		this.messages.add(new Message(MessageTypes.INFO, "The end of log " + this.logName));
	}

	public void printLog() {
		Iterator<Message> iterator = this.messages.iterator();
		while (iterator.hasNext()) {
			Message message = iterator.next();
			switch (message.getMessageType()) {
			case MessageTypes.ERROR:
				System.err.println(message.getMessageText());
				break;
			case MessageTypes.INFO:
				System.out.println(message.getMessageText());
				break;
			default:
				break;
			}
		}
	}

}

class Message {
	private final char messageType;
	private final String messageText;

	public Message(char messageType, String messageText) {
		this.messageType = messageType;
		this.messageText = messageText;
	}

	public char getMessageType() {
		return messageType;
	}

	public String getMessageText() {
		return messageText;
	}

}
