package exceptions;

public class DivisionByZero {

	public static void main(String[] args) {
		float a = 3;
		float b = 0;

		float result = a / b;
		System.out.println(result);

		try {
			int aI = 2;
			int bI = 0;

			int resultI = aI / bI;
		} catch (Exception e) {
			// show exceptions
			System.out.println(e.getMessage() + "\n" + e.getCause());
		}

//		} catch (NullPointerException e) {////// We tried to handle the wrong exception
//			System.out.println("Null Pointer Exception occured");
//		} catch (ArithmeticException e) {
//			System.out.println("Arithmetic Exception occured");
//		} finally {
//			System.out.println("Finally block is executed");
//		}

	}

}
