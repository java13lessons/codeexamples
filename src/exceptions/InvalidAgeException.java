package exceptions;

import inheritance.Person;

public class InvalidAgeException extends PersonException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidAgeException(Person person) {
		super(person);
	}

	@Override
	public String getIssueText() {
		return "The person's age " + this.person.getAge() + " is not valid for voting";
	}

	@Override
	public void printIssue() {
		System.err.println("The person's age " + this.person.getAge() + " is not valid for voting");
	}

}
