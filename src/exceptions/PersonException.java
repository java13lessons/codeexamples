package exceptions;

import inheritance.Person;

public abstract class PersonException extends Exception {

	protected Person person;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PersonException(Person person) {
		this.person = person;
	}

	public abstract void printIssue();
	public abstract String getIssueText();

}
