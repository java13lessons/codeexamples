package exceptions;

public interface MessageTypes {
	public static final char ERROR = 'E', WARNING = 'W', INFO = 'I';
}
