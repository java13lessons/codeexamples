package exceptions;

import inheritance.Person;

public class WrongPlaceException extends PersonException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WrongPlaceException(Person person) {
		super(person);
	}

	@Override
	public String getIssueText() {
		return "The person is registered in " + this.person.getPlace() + ". Voting here is not allowed";
	}

	@Override
	public void printIssue() {
		System.err.println("The person is registered in " + this.person.getPlace() + ". Voting here is not allowed");
	}

}
