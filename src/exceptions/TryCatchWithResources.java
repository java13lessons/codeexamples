package exceptions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;

public class TryCatchWithResources {

	public static void main(String[] args) {
		try (BufferedReader br = new BufferedReader(new FileReader(""))) {
			String var;
		} catch (ParseException | IOException exception) {
//			var = "2";///Cannot access the varibale, because it is defined in separate block
//			br.close(); OK
		}
	}

}
