package exceptions;

import inheritance.Person;

public class PersonHasVoted extends PersonException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PersonHasVoted(Person person) {
		super(person);
	}

	@Override
	public String getIssueText() {
		return "The person " + this.person.getName() + " " + this.person.getSurname() + " has already voted";
	}

	@Override
	public void printIssue() {
		System.err
				.println("The person " + this.person.getName() + " " + this.person.getSurname() + " has already voted");

	}
}
