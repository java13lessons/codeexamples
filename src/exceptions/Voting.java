package exceptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import inheritance.Date;
import inheritance.Person;

////This is the voting which is happening in one place
public class Voting {

	static private List<String> votes = new ArrayList<String>();
	static private Set<String> candidates = new TreeSet<String>();//// I want to have the unique names, so we can use
																	//// Set
	static private java.util.Scanner sc;
	static private final String VOTING_PLACE = "The first grammar school";
	static private final int VOTING_AGE = 18;
	private Person voter;
	static private MyLogingClass log;

	//// Static constructor, to be called before the class is accessed for the first
	//// time
	static {
		Voting.fillCandidates();
	}

	private static void fillCandidates() {
		Voting.candidates.add("Austen Yang");
		Voting.candidates.add("Roman Lowry");
		Voting.candidates.add("Rees Buck");
		Voting.candidates.add("Karishma Mcculloch");
		Voting.candidates.add("Jordon Kennedy");
		Voting.candidates.add("Menachem Blankenship");
	}

	public void start() {

		do {
			try {
				System.out.println("Please vote:");
				this.vote(sc.nextLine());//// we call the method which points us that it throws the exception
			} catch (PersonException e) {
				Voting.log.addMessage(MessageTypes.ERROR, e.getIssueText());
//				e.printIssue();
				break;//// since in this case there is no chance to vote, we need to break the voting
			} catch (WrongCandidateException e) {//// If the candidate name is wrong, we can try again
//				e.printIssue();/// In this case we can try to repeat
				Voting.log.addMessage(MessageTypes.ERROR, e.getIssueText());
			}
		} while (!this.voter.isVoted());

	}

	public Voting(Person voter) {
		this.voter = voter;
	}

	public void vote(String candidate) throws PersonException, WrongCandidateException {
		this.validate(candidate);
		Voting.votes.add(candidate);
		this.voter.setVoted();
		Voting.log.addMessage(MessageTypes.INFO,
				this.voter.getName() + " " + this.voter.getSurname() + " voted for " + candidate);
		System.out.println("Thank you!");
	}

	public static void main(String[] args) {
		/// initialize log at the start of the program
		Voting.log = new MyLogingClass("Voting log");
		sc = new java.util.Scanner(System.in);
		Person me = new Person("Arturs", "Olekss", new Date(15, 11, 1990));
		me.setPlace("The first grammar school");

		Voting voting = new Voting(me);
		voting.start();
		voting.start();
		sc.close();
		Voting.log.addEndMessage();
		Voting.log.printLog();
	}

	private void validate(String candidate) throws PersonException, WrongCandidateException {

		if (this.voter.isVoted())
			throw new PersonHasVoted(this.voter);//// throw exception also includes the return, if we don't have
													//// exception handling (try,catch) inside the method

		if (this.voter.getAge() < VOTING_AGE)
			throw new InvalidAgeException(this.voter);

		if (!this.voter.getPlace().equals(VOTING_PLACE))
			throw new WrongPlaceException(this.voter);

		if (!Voting.candidates.contains(candidate))/// if candidate does not exist
			throw new WrongCandidateException(candidate);
	}

}
