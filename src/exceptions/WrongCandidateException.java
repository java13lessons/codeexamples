package exceptions;

public class WrongCandidateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String candidateNameProvided;

	public WrongCandidateException(String candidateName) {
		this.candidateNameProvided = candidateName;
	}

	public void printIssue() {
		System.err.println("The candidate " + this.candidateNameProvided + " is not in the candidates' list");
	}

	public String getIssueText() {
		return "The candidate " + this.candidateNameProvided + " is not in the candidates' list";
	}

}
