package operators;

public class References {

	public static void main(String[] args) {
		String firstString = "Abc";
		String secondString = "Abc";
		System.out.println(firstString == secondString); //// true

		secondString = "A" + "bc";
		System.out.println(firstString == secondString); /// true

		String part1 = "A";
		String part2 = "bc";
		secondString = part1 + part2;//// new reference is created, thus firstString != secondString
		System.out.println(firstString == secondString);/// false
		//// 1.NEVER COMPARE THE STRINGS THIS WAY
		///// To compare the Strings use .compare method
		System.out.println(firstString.equals(secondString));/// true

		String stra = new String("user");
		String strb = "user";
		System.out.println(stra == strb);/// false, beacause the reference of "user" string is not the same as the one
											/// created using new String("user")

	}

}
