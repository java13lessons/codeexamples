package operators;

public class ArrayExamples {

	public static void main(String[] args) {
		int array[] = new int[5];

		array[0] = 4;
		array[1] = 3;
		array[2] = 8;
		array[3] = 3;
		array[4] = 1;

//		int array2[] = array;///this is not the copying
//		array2[4] = -4;///// array2 stores the same reference as array does ---> if we change the
//						///// values of the second array, the changes will be reflected in the first
//						///// array as well
//		System.out.println("The first array:");
//		printArray(array);

		int array2[] = new int[array.length];//// before we copy the array, we must define a new object
		System.arraycopy(array, 0, array2, 0, array.length);
		array2[2] = 4;
		printArray(array);//// not the same as array2 at this point
		System.out.print(System.lineSeparator());
		printArray(array2);

		System.out.print(System.lineSeparator());

		/// Add new element to the end
		array2 = addElementToArrayEnd(array2, 20);
		printArray(array2);

		System.out.print(System.lineSeparator());

		/// Add new element to the beggining
		array2 = addElementToArrayStart(array2, -34);
		printArray(array2);
	}

	//// Add new element to the array (at the end)
	private static int[] addElementToArrayEnd(int[] array, int element) {
		int[] newArray = new int[array.length + 1];//// we want to add a new element => the size is + 1
		System.arraycopy(array, 0, newArray, 0, array.length);/// the length is array.length, because we copy only
																/// array.length elements
		newArray[array.length] = element; //// the last element, we add
		return newArray;
	}

	//// Add new element to the array (at the beggining)
	private static int[] addElementToArrayStart(int[] array, int element) {
		int[] newArray = new int[array.length + 1];//// we want to add a new element => the size is + 1
		System.arraycopy(array, 0, newArray, 1, array.length);/// the length is array.length, because we copy only
																/// array.length elements
		newArray[0] = element; //// the first element, we add
		return newArray;
	}

	private static void printArray(int array[]) {
		for (int value : array) {
			System.out.print(value);
			System.out.print(" ");
		}
	}
}
