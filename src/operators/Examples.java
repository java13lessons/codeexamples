package operators;

import java.math.BigInteger;

public class Examples {

	/// This works as the "static constructor". Will be called, once the class is
	/// accessed or the first time
	static {
		System.out.println("The code from the static section");
	}

	public static void main(String[] args) {
		int a = 3, b = 23;
//		System.out.println(++a);
//		System.out.println(a++);
//		System.out.println(a);

//		int VariableName  the name of the variable should not start with uperrcase letter
		char varA;
		char vara; //// varA is no the same as vara

		/**
		 * <---- Start of JavaDoc comment int result = b % a; System.out.println("b % a
		 * = " + result);//// 23 / 5 = 4 + 3/5
		 * 
		 * final String APPLIED_BEHAVIOUR_ANALYIS = "AbA";
		 * 
		 * String type = APPLIED_BEHAVIOUR_ANALYIS;
		 * 
		 * String text = "\u00D8";// U+00D8 System.out.println(text);
		 * 
		 * String emoji = "\u1F602"; System.out.println(emoji);
		 * 
		 * String charactherUni = "\u01B3"; System.out.println(charactherUni);
		 */

		//// Comentings
		/*
		 * <---Start of the comment int result = b % a; System.out.println("b % a = " +
		 * result);//// 23 / 5 = 4 + 3/5
		 * 
		 * final String APPLIED_BEHAVIOUR_ANALYIS = "AbA";
		 * 
		 * String type = APPLIED_BEHAVIOUR_ANALYIS;
		 * 
		 * String text = "\u00D8";//U+00D8 System.out.println(text);
		 * 
		 * String emoji = "\u1F602"; System.out.println(emoji);
		 * 
		 * String charactherUni = "\u01B3"; System.out.println(charactherUni);
		 */

		int var;

//		System.out.println(var); //cannot use the variable, before it is initialized

//		double doubleVal = 2.34d;
//		
//		int intVal = (int) doubleVal;////We must point that the conversion to be done here
//		
//		doubleVal = intVal;
//		System.out.println(doubleVal);

		Double doubleVal = 2.34d;
//		Integer intVal = (Integer) doubleVal; --- we attempt to cast Double to Integer; 
		///// FOR REFERENCE TYPE, WHEN WE DO THE CASTING THE VALUE (THE REFERENCE) WILL
		///// NOT BE CHANGED!!!
		double doublePrimitiveVal = doubleVal;
		Integer intVal = (int) doublePrimitiveVal;///// ACTUALLY A NEW VALUE IS CREATED (when we do (int)
													///// doublePrimitiveVal )

		char charval = 'x';
		int intval = charval; // correct, intval will hold value // of Unicode character
		System.out.println(intval);
		double z = 12.414F;
		// // correct double is larger than float
		int z1 = (int) z; // will work, but
							// digits after // decimal point will be lost
		long l = 999999999999999999l;
		double d = (double) l; // will work, but digits before
		// decimal point will be lost

		int value = 4;

		String result = value < 4 ? "value is less then 4" : "value is greater or equal 4";

		// The same as if we write
		if (value < 4)
			result = "value is less then 4";
		else
			result = "value is greater or equal 4";

		int x, y, u;
		x = y = u = 100;//// x == 100; y == 100; u == 100

		//// This is the class, which works as the usual class in Java, not like Integer
		//// class or int type
		//// BigInteger is the class we can use to work with very large range of integer
		BigInteger veryLargeInteger1 = new BigInteger("4823948327432482524823413481743912039211284194812842141");

//		Integer intVal = veryLargeInteger1; ---- you can't pass BigInteger to Integer, since both are the object (use the clases) and the clases are not related with each other

		BigInteger veryLargeInteger2 = new BigInteger("94238409384934837448194812481204821048210484021804821");
		BigInteger sumResult = veryLargeInteger1.add(veryLargeInteger2);
		System.out.println(sumResult);

	}

}
