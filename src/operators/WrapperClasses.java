package operators;

public class WrapperClasses {

	public static void main(String[] args) {
		//// For each primitive type in Java we have the corresponding wrapper class
		int val = 2147483647;///// Maximal value of int type
//		val += 1; ///= -2147483648
//		val += 10; ///= -2147483639  ---> if the value is out of range, it starts from the smallest integer
//		System.out.println(val);
		Integer intVal = 2147483647;/// this will work the same way as int work
		intVal += 1;
		System.out.println(intVal);

		Double valD = 3.14d;
		Character valC = 'j';
		Float vaF = 3.14F;
		Boolean valB = true;

//		1 != '1'
		// 1 != "1"
		/// '1' != "1"

		//// 1.Wrapper types have more functionality (the methods)
		//// 2.Wrapper can be used as the type for Generics

	}

}
