package inheritance;

public class Date {

	//// Not nessesary, but we can indicate that we won't change the date after it
	//// is initialized, using final keyword
	private final int day;
	private final int month;
	private final int year;

	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

	@Override
	public String toString() {
		return this.day + "/" + this.month + "/" + this.year;
	}

}
