package inheritance;

import java.util.ArrayList;
import java.util.List;

public class ExampleInstanceOff {

	private static final List<Candies> boxOfCandies = new ArrayList<Candies>();
	private static final List<Toy> boxOfToys = new ArrayList<Toy>();

	public static void main(String[] args) {
		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("How many items do you want to add?");
		int numberOfItems = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < numberOfItems; i++) {
			System.out.println("What item do you want to enter?");
			String item = sc.nextLine();
			Object itemObject = getItemObject(item);
//			boxOfCandies.add(itemObject);////NOT CORRECT, because compiler does not know what's the type of Object
			if (itemObject instanceof Candies)/// after this check WE CAN BE SURE, THAT itemObject belongs to the class
												/// (or any sub-class of) Candies class
				boxOfCandies.add((Candies) itemObject);/// the compiler does not understand that object is a subclass of
														/// candies class, but we are sure, because of the check we have
														/// above
			else if (itemObject instanceof Toy)
				boxOfToys.add((Toy) itemObject);
			else
				System.out.println("Such item cannot be added");
		}
		System.out.println("The box of candies has " + boxOfCandies.size() + " items");
		System.out.println("The box of toys has " + boxOfToys.size() + " items");

		sc.close();

	}

	private static Object getItemObject(String itemName) {
		switch (itemName) {
		case "Teddy Bear":
			return new TeddyBear();
		case "Skittles":
			return new Skittles();
		case "Fruity Skittles":
			return new FruitSkittles();
		case "Candies Mix":
			return new Candies();
		default:
			return null;
		}

	}

}
