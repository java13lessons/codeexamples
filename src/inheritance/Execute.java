package inheritance;

public class Execute {

	public static void main(String[] args) {
		Director director = new Director("Lauren", "John", new Date(12, 1, 1990), "IT");
		director.setDefaultSalary();
		director.increaseAllowance();
		System.out.print(director);
	}

}
