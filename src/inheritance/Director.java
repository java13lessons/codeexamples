package inheritance;

import java.util.ArrayList;
import java.util.List;

public class Director extends Manager {

	private int carAllowance = 0;
	private static int ADD_SALARY = 500;
	public Director(String name, String surname, Date dateOfBirth, String department) {
		super(name, surname, dateOfBirth, department);
	}

	public Director(String name, String surname, int salary, Date dateOfBirth, String department) {
		super(name, surname, salary, dateOfBirth, department);
	}

	public Director(String name, String surname, Date dateOfBirth, String department, int carAllowance) {
		super(name, surname, dateOfBirth, department);
		this.carAllowance = carAllowance;
	}

	public Director(String name, String surname, int salary, Date dateOfBirth, String department, int carAllowance) {
		super(name, surname, salary, dateOfBirth, department);
		this.carAllowance = carAllowance;
	}

	public void increaseAllowance() {
		this.carAllowance++;
	}

	@Override
	public int getDefaultSalary() {
		return super.getDefaultSalary() + Director.ADD_SALARY;
	}

	/// Don't need to override it, because the logic is the same in the superclass,
	/// but PLEASE NOTE if you access to the method using this., then it will call
	/// the implemented you override in the object
//	@Override
//	public void setDefaultSalary() {
//		this.salary = this.getDefaultSalary();
//	}

	@Override
	public String toString() {
		return super.toString() + System.lineSeparator() + "Number of cars allowed to park: " + this.carAllowance;
	}

}
