package inheritance;

public class Engineer extends Employee {
	public Engineer(String name, String surname, Date dateOfBirth) {
		super(name, surname, dateOfBirth);
	}

	public Engineer(String name, String surname, int salary, Date dateOfBirth) {
		super(name, surname, salary, dateOfBirth);
	}

}
