package inheritance;

public class Toy {

	protected String name;////We will need to access the name inside the constructor of TeddyBear class
	private String category;
	private int weight;

	public String getCategory() {
		return category;
	}

	public String getName() {
		return name;
	}

	public int getWeight() {
		return weight;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

}
