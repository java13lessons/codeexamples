package inheritance;

public class Secretary extends Employee {

	public Secretary(String name, String surname, Date dateOfBirth) {
		super(name, surname, dateOfBirth);
	}

	public Secretary(String name, String surname, int salary, Date dateOfBirth) {
		super(name, surname, salary, dateOfBirth);
	}

}
