package inheritance;

public class Candies {

	protected String name;
	private int calories;
	private int grammsOfSugar;
	protected String flavour;

	public String getFlavour() {
		return flavour;
	}

	public void setFlavour(String flavour) {
		this.flavour = flavour;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCalories(int calories) {
		this.calories = calories;
	}

	public void setGrammsOfSugar(int grammsOfSugar) {
		this.grammsOfSugar = grammsOfSugar;
	}

	public int getCalories() {
		return calories;
	}

	public int getGrammsOfSugar() {
		return grammsOfSugar;
	}

	public String getName() {
		return name;
	}

}
