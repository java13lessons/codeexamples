package inheritance;

public class Employee extends Person {

	protected int salary;// since we will need to change it in the sub-classes, we need to define it as
							// protected
	private Manager boss;/// if employee is the manager - boss should be null

	private static final int BASE_SALARY = 1500;

//	/// If we define this as private, it means that we can not create the Employee
//	/// without provided the salary
//	private Employee(String name, String surname) {
//		super(name, surname);
//	}
//
//	public Employee(String name, String surname, int salary) {
//		this(name, surname);
//	}

	public Manager getBoss() {
		return boss;
	}

	public void setBoss(Manager boss) {
		this.boss = boss;
	}

	public Employee(String name, String surname, Date dateOfBirth) {
		super(name, surname, dateOfBirth);
	}

	public Employee(String name, String surname, int salary, Date dateOfBirth) {
		super(name, surname, dateOfBirth);
		this.salary = salary;
	}

	public void setDefaultSalary() {
		this.salary = this.getDefaultSalary();
	}

	public int getDefaultSalary() {
		return Employee.BASE_SALARY;
	}

	@Override
	public String toString() {
		return super.toString() + System.lineSeparator() + "Salary:" + this.salary;
	}

}
