package inheritance;

public class Person {
	//// Please, note that we don't define name and surname as protected
	///// Because we don't need to access them directly from the sub-classes, but
	//// please, remember that we can access them from the sub-classes using public
	//// and protected method
	/// In theory, we can define these attributes as final as well, because we won't
	//// change then after the initialization
	private String name;
	private String surname;
	private Date dateOfBirth;
	private boolean voted = false;
	private static final int CURRENT_YEAR = 2021;
	private String place;//// Place where the person is registered

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public int getAge() {
		return (CURRENT_YEAR - this.dateOfBirth.getYear());
	}

	public boolean isVoted() {
		return voted;
	}

	public void setVoted() {
		this.voted = true;
	}

	public Person(String name, String surname, Date dateOfBirth) {
		this.name = name;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	@Override
	public String toString() {
		return "Name:" + this.getName() + System.lineSeparator() + "Surname:" + this.getSurname()
				+ System.lineSeparator() + "Date of birth: " + this.getDateOfBirth();
	}
}
