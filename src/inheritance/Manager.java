package inheritance;

import java.util.ArrayList;
import java.util.List;

public class Manager extends Employee {

	private String department;
	private static final int ADD_SALARY = 500;

	private List<Employee> subordinates = new ArrayList<Employee>();

	public List<Employee> getSubordinates() {
		return subordinates;
	}

	public Manager(String name, String surname, Date dateOfBirth, String department) {
		super(name, surname, dateOfBirth);
		this.department = department;
	}

	public Manager(String name, String surname, int salary, Date dateOfBirth, String department) {
		super(name, surname, salary, dateOfBirth);
		this.department = department;
	}

	//// For manager we may have different salary
//	@Override
//	public void setDefaultSalary() {
//		// TODO Auto-generated method stub
//		this.salary = this.getDefaultSalary();
//	}

	@Override
	public int getDefaultSalary() {
		return super.getDefaultSalary() + Manager.ADD_SALARY;
	}

	public String getDepartment() {
		return department;
	}

	@Override
	public String toString() {
		return super.toString() + System.lineSeparator() + "Department: " + this.department;
	}

}
