package inheritance;

import java.util.ArrayList;
import java.util.List;

//////Bank may be the subclass of the Corporation or/and Company, BUT NOT THE SUBCLASS OF THE BANKING SYSTEM!!!
///Please, remember that we can't say that "Bank is Banking system"
public class Bank {

	private String name;
	private Director director; //// Director is used as the object, which mostly stores the data
	private List<Employee> employees = new ArrayList<Employee>();

	//// For collection we don't normally need the setters, because the collections
	//// are object and we normally just add or remove the elements from them
	//// But the setters are used to change/replace the object
	public List<Employee> getEmployees() {
		return employees;
	}

	public void setDirector(Director director) {
		this.director = director;
	}

	public void fireDirector() {
		this.director = null;
	}

	public Director getDirector() {
		return director;
	}

	public Bank(String name) {
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
